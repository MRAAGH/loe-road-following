# LoE road following

![figure 2](https://mazie.rocks/files/ai.gif)

![](https://files.mazie.rocks/2020-09-03_08-59-33.webm)

# FAQ

### How does this work?
Captures the screen and sends corresponding keypresses to your LoE game windows in real time.

### Is this allowed?
It is not against the rules. But moderators can always decide on a case-by-case basis, so it depends on who you ask.

### Is this a modded client?
No.

### Does this use AI?
No.

### So you programmed all the logic by hand?
By hoof!

### What about when it gets stuck on a bench or something?
It detects that and jumps over.

### What about when it gets stuck on an invisible wall?
It detects that and turns around.

### What about when it gets stuck really bad?
It detects that and runs /stuck.

### What about when it enters a new room?
It detects that and resizes the minimap and continues.

### What about when it gets into the evershade?
It detects that and runs /stuck until it gets back out.

### What about if it gets scrolled?
It detects that and closes the scroll.

### What about if it gets disconnected?
It logs in again.

### What about the Amareicas and Europonia buttons? They're randomized
It recognizes which button is Amareicas and clicks it.

### Is there any way it can get stuck?
If the game crashes, if it gets an un-closable scroll or if it gets into the Gala Hall

### How many can it run?
From 1 to 8 ponies at once, but this can easily be expanded. You need a lot of RAM and a lot of 1920x1080 screens.

### What do I need for this?
- Linux or Unix-like
- a window manager that supports window tiling, removing title bars and hiding screen statusbar (window list)
- multiple Legends of Equestria accounts
- at least one 1920x1080 (full HD) screen
- Python
- xdotool
- scrot
- Please keep in mind that this program will take control of both your keyboard and mouse, so you won't be able to use the computer normally while it's running.

### Can I do this on Windows?
You can try. You will need to rewrite the program in a new way. Don't expect it to work easily.

### So which window manager should I pick?
I have used this both in i3 window manager and in awesome window manager. But you don't need something so "hardcore". Any desktop environment that's configurable enough to allow completely removing window borders is probably fine.

### How do I use this?
1. open Legends of Equestria 4 times and log in with different accounts
2. position the 4 windows so that each takes up exactly a quarter of your left-most screen, without window decorations and without the operating system's bar. Please keep in mind that there is no room for error. If you misplace the window by just 1 pixel, this program will not work. You can't even have a 1-pixel border on the window.
3. find the window ID of each window (you can do this with the command `xdotool getactivewindow`)
4. put all 4 window IDs into the file `~/.marked_loe_windows` in correct order: first top left, then top right, then bottom left, then bottom right. Example file content:
```
23068692
18874388
20971540
16777236
```
I suggest binding the command `xdotool getactivewindow >>~/.marked_loe_windows` to a keyboard combination so you can trigger it while focusing the LoE window.

5. ensure Linux temporary files are stored in RAM, otherwise this will run quite slowly
6. For best results, make sure your texture quality is set to maximum and the chatbox is in the bottom right corner and your account only has 1 character in it.
7. run the program `loe-guard`

### Can't I do this without having 1920x1080 screens?
You could probably use Xephyr or a similar sandbox to get a virtual screen of the resolution you want. An additional benifit would be that the sandbox would have its own mouse cursor, so my program wouldn't take control of your *real* mouse any more. I haven't gotten far with this though. All I can say is, if you want to run games in Xephyr, you'll want to use virtualgl as described [here](www.reddit.com/r/archlinux/comments/1cxhqj/multiseat_gaming_guide/). And expect mouse camera controls to be broken in Xephyr.
